package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class HandleRenewal implements Strategy {

	@Override
	public boolean activate() {
		return Variables.needRenewal;
	}

	@Override
	public void execute() {
		Item[] i = Inventory.getItems(14290, 14288, 15124, 15122, 15120);
		
		if (i.length > 0) {
			if (i != null) {
				i[0].interact(Option.CONSUME);
				Time.sleep(1200);
				Variables.needRenewal = false;
			}
		}		
	}
}
