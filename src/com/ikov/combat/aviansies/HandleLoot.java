package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.GroundItem;
import org.rev317.min.api.wrappers.Item;

public class HandleLoot implements Strategy {
	
	@Override
	public boolean activate() {
		GroundItem[] toPickup = GroundItems.getNearest(Constants.LOOT);
		return toPickup.length > 0 && toPickup[0] != null;
	}

	@Override
	public void execute() {		

		final GroundItem g = GroundItems.getNearest(Constants.LOOT)[0];
		
		if (Inventory.isFull() && Inventory.contains(Constants.SHARK)) {
			Item i = Inventory.getItem(Constants.SHARK);
			if (i != null) {
				i.interact(Option.CONSUME);
				Time.sleep(1200);
			}
		}
		if (g != null) {
			Menu.sendAction(234, g.getId(), g.getX(), g.getY());
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return g == null;
				}
			}, 2000);
		}
		
	}
}
