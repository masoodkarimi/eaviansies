package com.ikov.combat.aviansies;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.rev317.min.Loader;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;


@ScriptManifest(author = "Empathy", category = Category.COMBAT, description = "Aviansie Killer", name = "EAviansies", servers = { "Ikov" }, version = 2.0)
public class Core extends Script implements MessageListener {

	public boolean onExecute() {
		provide(Arrays.asList(new HandleLogin(), new HandleBanking(), new HandleRenewal(), new HandleEating(), new HandleLoot(), new HandleCombat(), new HandleTravel()));
		return true;
	}

	@Override
	public void messageReceived(MessageEvent m) {
		if (m.getType() == 0) {
			if (m.getMessage().contains("object does not exist") || m.getMessage().contains("is already on your") || m.getMessage().contains("clan chat")) {
				dropClient();
			} else if (m.getMessage().contains("renewal has run")) {
				Variables.needRenewal = true;
			}
		}
	}


	private void dropClient() {
		try {
			Method dropClient = Loader.getClient().getClass().getDeclaredMethod("W");
			dropClient.setAccessible(true);
			dropClient.invoke(Loader.getClient());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}