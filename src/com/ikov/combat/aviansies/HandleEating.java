package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class HandleEating implements Strategy {

	@Override
	public boolean activate() {
		return Skill.HITPOINTS.getLevel() < 60;
	}

	@Override
	public void execute() {
		Item i = Inventory.getItem(Constants.SHARK);
		
		if (i != null) {
			i.interact(Option.CONSUME);
			Time.sleep(1200);
		}		
	}
}
