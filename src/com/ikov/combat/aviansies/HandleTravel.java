package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.methods.SceneObjects.Option;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;

public class HandleTravel implements Strategy {

	@Override
	public boolean activate() {
		return !Methods.atAviansies();
	}

	@Override
	public void execute() {

		for (int i = 0; i < Constants.TELEPORT.length; i++) {
			Menu.sendAction(315, 0, 0, Constants.TELEPORT[i], 0);
			Time.sleep(1200);
		}

		Time.sleep(2000);

		final Tile firstTile = new Tile(2870, 5305);
		firstTile.walkTo();
		Time.sleep(new SleepCondition() {

			@Override
			public boolean isValid() {
				return firstTile.distanceTo() < 3;
			}

		}, 25000);

		final Tile secondTile = new Tile(2872, 5288);
		secondTile.walkTo();
		Time.sleep(new SleepCondition() {

			@Override
			public boolean isValid() {
				return secondTile.distanceTo() < 3;
			}

		}, 25000);

		final Tile thirdTile = new Tile(2871, 5279);
		thirdTile.walkTo();
		Time.sleep(new SleepCondition() {

			@Override
			public boolean isValid() {
				return Players.getMyPlayer().getLocation().equals(thirdTile);
			}

		}, 25000);

		SceneObject[] grapple = SceneObjects.getNearest(26303);
		try {
			if (grapple.length > 0) {
				if (grapple[0] != null) {
					grapple[0].interact(Option.FIRST);
					Time.sleep(new SleepCondition() {

						@Override
						public boolean isValid() {
							return Players.getMyPlayer().getLocation().equals(new Tile(2872, 5269));
						}

					}, 25000);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		final Tile fourthTile = new Tile(2859, 5263);
		fourthTile.walkTo();
		Time.sleep(new SleepCondition() {

			@Override
			public boolean isValid() {
				return fourthTile.distanceTo() < 3;
			}

		}, 25000);
	}
}
