package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

public class HandleBanking implements Strategy {

	@Override
	public boolean activate() {
		return Inventory.isFull() && !Inventory.contains(Constants.SHARK) || Methods.needFood();
	}

	@Override
	public void execute() {
		if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
			Methods.turnOff();
			Keyboard.getInstance().sendKeys("::home");
			Time.sleep(4000);
		}

		if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
			while (!Bank.isOpen()) {
				Bank.open(Bank.getBank());
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Bank.isOpen();
					}
				}, 3000);
			}
		}

		if (Bank.isOpen()) {

			Bank.depositAllExcept();

			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !Inventory.isFull();
				}

			}, 5000);

			Item prayer = Bank.getItem(Constants.PRAYER_RENEWAL);

			if (prayer != null) {
				Menu.sendAction(632, prayer.getId() - 1, prayer.getSlot(), 5382, 2213, 7);
				Time.sleep(500);
			}

			Item shark = Bank.getItem(Constants.SHARK);

			if (shark != null) {
				Menu.sendAction(431, shark.getId() - 1, shark.getSlot(), 5382, 2213, 4);
				Time.sleep(500);
			}

			Bank.close();

		}

	}
}
