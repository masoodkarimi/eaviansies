package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Npcs.Option;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Npc;

public class HandleCombat implements Strategy {

	Npc monster = null;

	@Override
	public boolean activate() {
		return Methods.atAviansies();
	}

	@Override
	public void execute() {
		Methods.turnOn();

		if (monster != null) {
			if (Players.getMyPlayer().getInteractingCharacter() != null) {
				if (Players.getMyPlayer().getInteractingCharacter().equals(monster)) {
					Time.sleep(1000);
				}
			} else {
				monster = null;
			}
		} else {
			final Npc[] monsterToAttack = Npcs.getNearest(monsterFilter);

			if (monsterToAttack.length > 0)
				monster = monsterToAttack[0];

			if (monster != null && monster.getDef() != null) {
				monster.interact(Option.ATTACK);
			}
			Time.sleep(2000);
		}
	}

	private final Filter<Npc> monsterFilter = new Filter<Npc>() {
		@Override
		public boolean accept(Npc n) {
			if (n != null && n.getDef().getId() == 6232 && !n.isInCombat()) {
				return true;
			}
			return false;
		}

	};
}
