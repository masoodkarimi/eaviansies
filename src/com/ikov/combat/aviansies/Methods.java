package com.ikov.combat.aviansies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Tile;

public class Methods {

	public static boolean needFood() {
		return !Inventory.contains(Constants.SHARK);
	}

	public static boolean atAviansies() {
		return new Tile(2859, 5263).distanceTo() < 40;
	}
	
	public static void turnOn() {
		if (!isActivated()) {
			Menu.sendAction(169, -1, -1, 22519);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return isActivated();
				}
			}, 1000);
		}
	}
	
	public static void turnOff() {
		if (isActivated()) {
			Menu.sendAction(169, -1, -1, 22519);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !isActivated();
				}
			}, 1000);
		}
	}

	public static boolean isActivated() {
		return Game.getSetting(618) != 0;
	}
	
}
